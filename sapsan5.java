package sapsan;

public class sapsan5 {
	public static int Simple(int a) {
		int divisors = 0;
		for(int i = 2;i < 101;i++) {
			if(a%i == 0) {
				divisors++;
			}
		}
		return divisors;
	}
	public static void main(String[] args) {
		for(int i = 2; i < 101; i++) {
			int result = Simple(i);
			if(result == 1) {
				System.out.println(i);
			}
		}
	}
}
