import java.util.*;
public class Sapsan {
	public static int min(int a,int b, int c, int d) {
		 int first = Math.min(a, b);
		 int second = Math.min(c,d);
		 int result = Math.min(first, second);
		 return result;
	}
	 public static void main(String[] args) {
		 Scanner scanner = new Scanner(System.in);
		 int a = scanner.nextInt();
		 int b = scanner.nextInt();
		 int c = scanner.nextInt();
		 int d = scanner.nextInt();
		 System.out.println(min(a,b,c,d));
	 }
}
